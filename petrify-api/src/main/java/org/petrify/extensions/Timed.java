/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.extensions;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.petrify.core.DefaultTransitionDecorator;
import org.petrify.core.Transition;
import org.petrify.meta.MemberInfo;
import org.petrify.meta.TransitionDecorator;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@TransitionDecorator(Timed.Decorator.class)
public @interface Timed {

	String DELAY = "timed.delay";
	String UNIT = "timed.unit";

	int delay() default 1;

	TimeUnit unit() default TimeUnit.MILLISECONDS;
	
	class Decorator extends DefaultTransitionDecorator {

		private int delay;
		private TimeUnit unit;
		
		public Decorator(Transition transition, Timed anno, MemberInfo info) {
			super(transition);
			delay = anno.delay();
			unit = anno.unit();
		}

		@Override
		public void configure(Map<String, Object> configuration) {
			super.configure(configuration);
			if (configuration.containsKey(DELAY)) {
				delay = (Integer) configuration.get(DELAY);
				unit = (TimeUnit) configuration.get(UNIT);
			}
		}

		@Override
		protected void execute() throws InterruptedException {
				unit.sleep(delay);
				super.execute();
		}
		
	}
	
}
