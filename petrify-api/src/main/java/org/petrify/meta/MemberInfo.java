/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.meta;

import java.lang.reflect.Executable;
import java.lang.reflect.Parameter;

public class MemberInfo {
		
		private Class<?> owningClass;
		
		private Executable method;
		
		private Parameter parameter;
		
		public MemberInfo(Class<?> owningClass, Executable method, Parameter parameter) {
			super();
			this.owningClass = owningClass;
			this.method = method;
			this.parameter = parameter;
		}

		public Class<?> getOwningClass() {
			return owningClass;
		}
		
		public Executable getMethod() {
			return method;
		}
		
		public Parameter getParameter() {
			return parameter;
		}
		
	}