/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.meta;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.petrify.core.Transition;

/**
 * Meta-annotation for providing additional behavior to transitions.
 * 
 * @see Transition
 * @author pawel_kierat
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface TransitionDecorator {

	/**
	 * Implementation of the decorator. The class must provide a public
	 * constructor with the following arguments:<br>
	 * 1. the original transition instance (of type {@link Transition}),<br>
	 * 2. instance of the meta-annotated annotation.
	 */
	Class<? extends Transition> value();
	
}
