package org.petrify.core;

public interface Arc {

    Place getPlace();

    boolean isEnabled();

    boolean acquire() throws InterruptedException;

    void release();

}
