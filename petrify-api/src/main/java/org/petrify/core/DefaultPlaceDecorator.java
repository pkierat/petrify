/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.core;

public abstract class DefaultPlaceDecorator implements Place {

	protected final Place decorated;

	protected DefaultPlaceDecorator(Place decorated) {
		this.decorated = decorated;
	}

	@Override
	public String getName() {
		return decorated.getName();
	}

	@Override
	public boolean isEmpty() {
		return decorated.isEmpty();
	}

	@Override
	public boolean isFull() {
		return decorated.isFull();
	}

	@Override
	public boolean acquireForConsuming() throws InterruptedException {
		return decorated.acquireForConsuming();
	}

	@Override
	public boolean acquireForProducing() throws InterruptedException {
		return decorated.acquireForProducing();
	}

	@Override
	public Token consume() {
		return decorated.consume();
	}

	@Override
	public void produce(Token value) {
		decorated.produce(value);
	}

	@Override
	public void releaseForConsuming() {
		decorated.releaseForConsuming();
	}

	@Override
	public void releaseForProducing() {
		decorated.releaseForProducing();
	}

	@Override
	public int hashCode() {
		return decorated.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return decorated.equals(obj);
	}

	@Override
	public String toString() {
		return decorated.toString();
	}

}
