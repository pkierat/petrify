/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.core;

import java.util.List;
import java.util.Map;

public abstract class Transition {

	public Transition() {
		super();
	}

	public abstract String getName();

	public abstract List<InputArc> getInputArcs();
	
	public abstract List<OutputArc> getOutputArcs();

	public abstract void configure(Map<String, Object> configuration);

	public abstract boolean isEnabled();

	protected abstract boolean acquire() throws InterruptedException;
	
	protected abstract void consume();
	
	protected abstract void execute() throws InterruptedException;
	
	protected abstract void produce();
	
	protected abstract void release();

	public final void fire() throws InterruptedException {
		boolean enabled = false;
		try {
			enabled = acquire();
			if (enabled) {
				consume();
				execute();
				produce();
			}
		} finally {
			if (enabled) {
				release();
			}
		}
	}

}