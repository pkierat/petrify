/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.core;

import java.util.List;
import java.util.Map;

public abstract class DefaultTransitionDecorator extends Transition {

	protected final Transition decorated;
	
	protected DefaultTransitionDecorator(Transition decorated) {
		this.decorated = decorated;
	}

	@Override
	public void configure(Map<String, Object> configuration) {
		decorated.configure(configuration);
	}

	@Override
	public String getName() {
		return decorated.getName();
	}

	@Override
	public List<InputArc> getInputArcs() {
		return decorated.getInputArcs();
	}

	@Override
	public List<OutputArc> getOutputArcs() {
		return decorated.getOutputArcs();
	}
	
	@Override
	public boolean isEnabled() {
		return decorated.isEnabled();
	}
	
	
	@Override
	protected boolean acquire() throws InterruptedException {
		return decorated.acquire();
	}

	@Override
	protected void consume() {
		decorated.consume();
	}

	@Override
	protected void execute() throws InterruptedException {
		decorated.execute();
	}

	@Override
	protected void produce() {
		decorated.produce();
	}

	@Override
	protected void release() {
		decorated.release();
	}

	@Override
	public int hashCode() {
		return decorated.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return decorated.equals(obj);
	}

	@Override
	public String toString() {
		return decorated.toString();
	}
	
}
