package org.petrify.viewer;

import org.petrify.core.Net;
import org.junit.Ignore;
import org.junit.Test;
import org.petrify.sample.SamplePojoAction;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class NetViewerTest {

    @Test
    public void testReadEmptyStylesheet() {
        ByteArrayInputStream stream = new ByteArrayInputStream("".getBytes(StandardCharsets.UTF_8));
        String stylesheet = NetViewer.readStylesheet(stream);
        assertEquals("", stylesheet);
    }

    @Test
    public void testReadNotEmptyStylesheet() {
        ByteArrayInputStream stream = new ByteArrayInputStream("test".getBytes(StandardCharsets.UTF_8));
        String stylesheet = NetViewer.readStylesheet(stream);
        assertEquals("test", stylesheet);
    }

    @Test
    public void testNetViewer() throws Exception {
        Net net = new Net("org.petrify.sample.settlers");
        viewNet(net);
    }

    @Test
    public void testNetViewer2() throws Exception {
        Net net = new Net(Collections.singleton(SamplePojoAction.class));
        viewNet(net);
    }


    private void viewNet(Net net) throws InterruptedException {
        NetViewer viewer = new NetViewer(net);
        viewer.display();
        Thread.sleep(60000);
    }

}