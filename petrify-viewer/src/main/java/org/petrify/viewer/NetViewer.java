/*
 * Copyright 2018 Pawel Kierat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.viewer;

import java.io.InputStream;
import java.util.Scanner;

import org.petrify.core.Net;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

import static java.nio.charset.StandardCharsets.UTF_8;

public class NetViewer {

    private final Net net;

    private Graph graph;

    static {
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
    }

    public NetViewer(Net net) {
        this.net = net;
        buildGraph();
    }

    private void buildGraph() {
        graph = new SingleGraph(net.toString());
        InputStream css = NetViewer.class.getResourceAsStream("/style.css");
        String stylesheet = readStylesheet(css);
        graph.addAttribute("ui.stylesheet", stylesheet);

        net.getPlaces().forEach(place -> {
            String name = place.getName();
            Node node = graph.addNode(name);
            node.addAttribute("ui.class", "element, place");
            node.addAttribute("ui.label", name);
        });

        net.getTransitions().forEach(transition -> {
            String tname = transition.getName();
            Node node = graph.addNode(tname);
            node.addAttribute("ui.class", "element, transition");
            node.addAttribute("ui.label", tname);
            transition.getInputArcs().forEach(arc -> {
                String pname = arc.getPlace().getName();
                Edge edge = graph.addEdge(pname + "->" + tname, pname, tname, true);
                edge.addAttribute("ui.class", "arc");
            });
            transition.getOutputArcs().forEach(arc -> {
                String pname = arc.getPlace().getName();
                Edge edge = graph.addEdge(tname + "->" + pname, tname, pname, true);
                edge.addAttribute("ui.class", "arc");
            });
        });
    }

    public void display() {
        graph.display(true);
    }

    static String readStylesheet(InputStream istream) {
        try (Scanner scanner = new Scanner(istream, UTF_8.name()).useDelimiter("\\A")) {
            return scanner.hasNext() ? scanner.next() : "";
        }
    }

}
