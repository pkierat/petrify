package org.petrify.core.sync;

public interface Synchronizer {

    boolean acquireForConsuming() throws InterruptedException;

    void releaseForConsuming();

    boolean acquireForProducing() throws InterruptedException;

    void releaseForProducing();

}
