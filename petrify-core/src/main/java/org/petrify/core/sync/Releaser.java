package org.petrify.core.sync;

@FunctionalInterface
public interface Releaser<T> {

    void release(T sync);

}
