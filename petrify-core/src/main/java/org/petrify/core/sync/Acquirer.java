package org.petrify.core.sync;

@FunctionalInterface
public interface Acquirer<T> {

    boolean acquire(T sync) throws InterruptedException;

}
