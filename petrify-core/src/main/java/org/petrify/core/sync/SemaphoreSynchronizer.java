package org.petrify.core.sync;

import java.util.concurrent.Semaphore;

public class SemaphoreSynchronizer implements Synchronizer {

    private final Semaphore readPermits;
    private final Semaphore writePermits;
    private final AcquiringStrategy strategy;

    private SemaphoreSynchronizer(int capacity, AcquiringStrategy acquiringStrategy) {
        this.readPermits = new Semaphore(0, true);
        this.writePermits = new Semaphore(capacity, true);
        this.strategy = acquiringStrategy;
    }

    public static Synchronizer blocking(int capacity) {
        return new SemaphoreSynchronizer(capacity, AcquiringStrategy.BLOCKING);
    }

    public static Synchronizer nonBlocking(int capacity) {
        return new SemaphoreSynchronizer(capacity, AcquiringStrategy.NON_BLOCKING);
    }

    @Override
    public boolean acquireForConsuming() throws InterruptedException {
        return strategy.acquirer.acquire(readPermits);
    }

    @Override
    public boolean acquireForProducing() throws InterruptedException {
        return strategy.acquirer.acquire(writePermits);
    }

    @Override
    public void releaseForConsuming() {
        strategy.releaser.release(readPermits);
    }

    @Override
    public void releaseForProducing() {
        strategy.releaser.release(writePermits);
    }

    private enum AcquiringStrategy {

        BLOCKING((s) -> { s.acquire(); return true; }, Semaphore::release),
        NON_BLOCKING(Semaphore::tryAcquire, Semaphore::release);

        final Acquirer<Semaphore> acquirer;
        final Releaser<Semaphore> releaser;

        AcquiringStrategy(Acquirer<Semaphore> acquirer, Releaser<Semaphore> releaser) {
            this.acquirer = acquirer;
            this.releaser = releaser;
        }

    }

}