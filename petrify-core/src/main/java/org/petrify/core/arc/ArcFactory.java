/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.core.arc;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.petrify.core.*;
import org.petrify.core.sync.Acquirer;
import org.petrify.core.sync.Releaser;
import org.petrify.meta.InputArcDecorator;
import org.petrify.meta.MemberInfo;
import org.petrify.meta.OutputArcDecorator;

public final class ArcFactory {

	private ArcFactory() {
	}
	
	public static InputArc createInputArc(Place place, Consumer<Object> slot, Method method) {
		InputArc arc = new InputArcImpl(place, slot);
		MemberInfo memberInfo = new MemberInfo(method.getDeclaringClass(), method, null);
		return ReflectionUtils.decorate(InputArc.class, arc, InputArcDecorator.class,
				method.getAnnotations(), memberInfo);
	}

	public static InputArc createInputArc(Place place, Consumer<Object> slot, Parameter parameter) {
		InputArc arc = new InputArcImpl(place, slot);
		MemberInfo memberInfo = new MemberInfo(
				parameter.getDeclaringExecutable().getDeclaringClass(),
				parameter.getDeclaringExecutable(), parameter);
		return ReflectionUtils.decorate(InputArc.class, arc, InputArcDecorator.class,
				parameter.getAnnotations(), memberInfo);
	}

	public static OutputArc createOutputArc(Place place, Supplier<Object> slot, Method method) {
		OutputArc arc = new OutputArcImpl(slot, place);
		MemberInfo memberInfo = new MemberInfo(method.getDeclaringClass(), method, null);
		return ReflectionUtils.decorate(OutputArc.class, arc, OutputArcDecorator.class,
				method.getAnnotations(), memberInfo);
	}

	private static abstract class AbstractArc {
		
		protected final Place place;
		
		protected final Acquirer<Place> acquireMethod;
	
		protected final Releaser<Place> releaseMethod;
	
		protected boolean acquired;
		
		
		AbstractArc(Place place, Acquirer<Place> acquireMethod, Releaser<Place> releaseMethod) {
			this.place = place;
			this.acquireMethod = acquireMethod;
			this.releaseMethod = releaseMethod;
		}
		
		public Place getPlace() {
			return place;
		}
		
		
		public boolean acquire() throws InterruptedException {
			acquireMethod.acquire(place);
			return acquired = true;
		}
		
		public void release() {
			if (acquired) {
				releaseMethod.release(place);
				acquired = false;
			}
		}

		public abstract boolean isEnabled();
	
	}
	
	private static class InputArcImpl extends AbstractArc implements InputArc {
		
		final Consumer<Object> slot;
		
		InputArcImpl(Place place, Consumer<Object> slot) {
			super(place, Place::acquireForConsuming, Place::releaseForConsuming);
			this.slot = slot;
		}

		public void consume() {
			Token arg = place.consume();
			acquired = false;
			slot.accept(arg.getValue());
		}
		
		@Override
		public boolean isEnabled() {
			return !place.isEmpty();
		}

	}
	
	private static class OutputArcImpl extends AbstractArc implements OutputArc {

		final Supplier<Object> slot;
		
		OutputArcImpl(Supplier<Object> slot, Place place) {
			super(place, Place::acquireForProducing, Place::releaseForProducing);
			this.slot = slot;
		}

		@Override
		public void produce() {
			Object result = slot.get();
			if (result != null) {
				place.produce(new Token(result));
				acquired = false;
			} else {
				release();
			}
		}

		@Override
		public boolean isEnabled() {
			return !place.isFull();
		}

	}

}


