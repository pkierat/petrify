/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.core.transition;

import java.lang.invoke.MethodHandles;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.petrify.core.Transition;

public class TransitionRunner extends Thread {

	private static final Logger log = Logger.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());
	
	private final Transition transition;

	private volatile boolean terminate = false;

	public TransitionRunner(Transition transition) {
		super(transition.getName());
		this.transition = transition;
	}	

	@Override
	public void interrupt() {
		terminate = true;
		super.interrupt();
	}
	
	@Override
	public void run() {
		log.info("Thread '" + transition.getName() + "' started.");
		while (!terminate && !isInterrupted()) {
			try {
				transition.fire();
			} catch (InterruptedException e) {
				if (!terminate) {
					log.log(Level.SEVERE, "Thread '" + transition.getName() + "' interrupted", e);
					terminate = true;
				}
			} catch (Exception e) {
				log.log(Level.SEVERE, e.getMessage(), e);
			}
		}
		log.info("Thread '" + transition.getName() + "' stopped.");
	}

}