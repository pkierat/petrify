/*
 * Copyright 2018 Pawel Kierat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.core.transition;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.petrify.core.Transition;
import org.petrify.core.InputArc;
import org.petrify.core.OutputArc;

final class GenericTransition extends Transition {

    private final String name;

    private final InputArc[] inputArcs;

    private final OutputArc[] outputArcs;

    private final Runnable action;

    GenericTransition(String name, Runnable action, InputArc[] inputArcs, OutputArc[] outputArcs) {
        this.name = name;
        this.inputArcs = inputArcs;
        this.outputArcs = outputArcs;
        this.action = action;
    }

    @Override
    public void configure(Map<String, Object> configuration) { }

    public String getName() {
        return name;
    }

    @Override
    public List<InputArc> getInputArcs() {
        return Arrays.asList(inputArcs);
    }

    @Override
    public List<OutputArc> getOutputArcs() {
        return Arrays.asList(outputArcs);
    }

    @Override
    public boolean isEnabled() {
        boolean enabled = true;
        for (int i = 0; i < inputArcs.length && enabled; ++i) {
            enabled = inputArcs[i].isEnabled();
        }
        for (int i = 0; i < outputArcs.length && enabled; ++i) {
            enabled = outputArcs[i].isEnabled();
        }
        return enabled;
    }

    @Override
    protected boolean acquire() throws InterruptedException {
        boolean acquired = true;
        for (int i = 0, len = inputArcs.length; i < len && acquired; ++i) {
            acquired = inputArcs[i].acquire();
        }
        for (int i = 0, len = outputArcs.length; i < len && acquired; ++i) {
            acquired = outputArcs[i].acquire();
        }
        return acquired;
    }

    @Override
    protected void consume() {
        for (InputArc inputArc : inputArcs) {
            inputArc.consume();
        }
    }

    @Override
    protected void execute() {
        action.run();
    }

    @Override
    protected void produce() {
        for (OutputArc outputArc : outputArcs) {
            outputArc.produce();
        }
    }

    @Override
    protected void release() {
        for (int i = outputArcs.length - 1; i >= 0; --i) {
            outputArcs[i].release();
        }
        for (int i = inputArcs.length - 1; i >= 0; --i) {
            inputArcs[i].release();
        }
    }

}