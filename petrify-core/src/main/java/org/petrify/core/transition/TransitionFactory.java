/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.core.transition;

import static org.petrify.core.arc.ArcFactory.createInputArc;
import static org.petrify.core.arc.ArcFactory.createOutputArc;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.petrify.core.*;

import org.petrify.Input;
import org.petrify.Output;
import org.petrify.core.place.PlaceFactory;
import org.petrify.meta.MemberInfo;
import org.petrify.meta.TransitionDecorator;

public class TransitionFactory {

    private final Map<Class<?>, AtomicInteger> transitionNumbers;
    
    private TransitionFactory() {
        transitionNumbers = new HashMap<>();
    }

    public static TransitionFactory get() {
        return new TransitionFactory();
    }

    public Transition createTransition(Class<?> transitionClass, Map<String, Place> places) {
    	try {
            Object transition = transitionClass.getDeclaredConstructor().newInstance();
            return createTransition(transition, places);
        } catch (InstantiationException
				| IllegalAccessException
				| NoSuchMethodException
				| InvocationTargetException e) {
    	    throw new IllegalStateException(e);
        }
	}

	public Transition createTransition(Object actor, Map<String, Place> places) {
		Method actionMethod = ReflectionUtils
				.getFirstAnnotatedMethod(actor.getClass(), org.petrify.Action.class)
				.orElseThrow(Exceptions.missingAction());
		return createTransition(actor, actionMethod, places);
	}
	
	private Transition createTransition(Object actor, Method actionMethod, Map<String, Place> places) {

		final Parameter[] actionParameters = actionMethod.getParameters();
		final Action action = new Action(ReflectionUtils.getFunction(actor, actionMethod), actionParameters.length);
		final List<Method> inputMethods = ReflectionUtils.getAllAnnotatedMethods(actor.getClass(), Input.class);
		final List<Method> outputMethods = ReflectionUtils.getAllAnnotatedMethods(actor.getClass(), Output.class);
		
		List<InputArc> inputArcs = new ArrayList<>(actionParameters.length + inputMethods.size());
		List<OutputArc> outputArcs = new ArrayList<>(outputMethods.size() + 1);
		
		for (int i = 0; i < actionParameters.length; ++i) {
			Place place = createPlace(Input.class, actionParameters[i], places);
			Consumer<Object> slot = action.consumer(i);
			inputArcs.add(createInputArc(place, slot, actionParameters[i]));
		}
				
		if (actionMethod.getReturnType() != void.class) {
			Place place = createPlace(Output.class, actionMethod, places);
			Supplier<Object> slot = action.producer();
			outputArcs.add(createOutputArc(place, slot, actionMethod));
		}

		for (Method method : inputMethods) {
			if (method.equals(actionMethod)) { continue; }
			Place place = createPlace(Input.class, method, places);
			Consumer<Object> slot = ReflectionUtils.getConsumer(actor, method);
			inputArcs.add(createInputArc(place, slot, method));
		}

		for (Method method : outputMethods) {
			if (method.equals(actionMethod)) { continue; }
			Place place = createPlace(Output.class, method, places);
			Supplier<Object> slot = ReflectionUtils.getSupplier(actor, method);
			outputArcs.add(createOutputArc(place, slot, method));
		}

		Transition transition = new GenericTransition(getTransitionName(actor), action,
				inputArcs.toArray(new InputArc[]{}), outputArcs.toArray(new OutputArc[]{}));

		MemberInfo memberInfo = new MemberInfo(actor.getClass(), actionMethod, null);
		return ReflectionUtils.decorate(Transition.class, transition, TransitionDecorator.class,
				actor.getClass().getAnnotations(), memberInfo);
	}

	private Place createPlace(Class<? extends Annotation> type, AnnotatedElement element, Map<String, Place> places) {
		String placeName = getPlaceName(type, element);
		return places.computeIfAbsent(placeName, (name) -> PlaceFactory.createPlace(name, element.getAnnotations()));
	}
	
	private String getTransitionName(Object actor) {
		Class<?> actorClass = actor.getClass();
		org.petrify.Transition trans = actorClass.getAnnotation(org.petrify.Transition.class);
		return trans != null && trans.value().length() > 0 ? trans.value() : actorClass.getSimpleName();
	}

	private static String getPlaceName(Class<? extends Annotation> annoClass, AnnotatedElement element) {
		return Arrays.stream(element.getAnnotations())
				.filter(a -> a.annotationType() == annoClass || a.annotationType().isAnnotationPresent(annoClass))
				.findFirst().map(a -> ReflectionUtils.getAnnotationValue(a, "value"))
				.map(Object::toString).filter(not(String::isEmpty))
				.orElseThrow(Exceptions.missingPlaceName(element.toString()));
	}

	private static <T> Predicate<T> not(Predicate<T> predicate) {
    	return predicate.negate();
	}

	private static class Action implements Runnable {
		
		final Function<Object[], Object> function;
		
		final Object[] args;
		
		Object result;
	
		Action(Function<Object[], Object> function, int arity) {
			this.function = function;
			args = new Object[arity];
		}

		@Override
		public void run() {
			result = function.apply(args);
		}
		
		Consumer<Object> consumer(final int i) {
			return (arg) -> args[i] = arg;
		}
		
		Supplier<Object> producer() {
			return () -> result;
		}
		
	}

}
