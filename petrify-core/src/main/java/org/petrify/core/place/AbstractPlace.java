/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.core.place;

import org.petrify.core.Place;
import org.petrify.core.Token;
import org.petrify.core.sync.SemaphoreSynchronizer;
import org.petrify.core.sync.Synchronizer;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import static org.petrify.core.sync.SemaphoreSynchronizer.blocking;
import static org.petrify.core.sync.SemaphoreSynchronizer.nonBlocking;

public abstract class AbstractPlace implements Place {

    private static final int INITIAL_CAPACITY = 16;

    private final String name;
    private final int capacity;
    private final Queue<Token> tokens;
    private final Synchronizer synchronizer;

    protected AbstractPlace(String name) {
        this(name, INITIAL_CAPACITY, true);
    }

    protected AbstractPlace(String name, int capacity, boolean blocking) {
        this(name, capacity, blocking ? blocking(capacity) : nonBlocking(capacity));
    }

    protected AbstractPlace(String name, int capacity, Synchronizer synchronizer) {
        this.name = name;
        this.capacity = capacity;
        this.tokens = new ConcurrentLinkedQueue<>();
        this.synchronizer = synchronizer;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isEmpty() {
        return tokens.isEmpty();
    }

    @Override
    public boolean isFull() {
        return tokens.size() == capacity;
    }

    @Override
    public boolean acquireForConsuming() throws InterruptedException {
        return synchronizer.acquireForConsuming();
    }

    @Override
    public boolean acquireForProducing() throws InterruptedException {
        return synchronizer.acquireForProducing();
    }

    @Override
    public void releaseForConsuming() {
        synchronizer.releaseForConsuming();
    }

    @Override
    public void releaseForProducing() {
        synchronizer.releaseForProducing();
    }

    @Override
    public Token consume() {
        Token token = tokens.remove();
        synchronizer.releaseForProducing();
        return token;
    }

    @Override
    public void produce(Token value) {
        tokens.add(value);
        synchronizer.releaseForConsuming();
    }

}