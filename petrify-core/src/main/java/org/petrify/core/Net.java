/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.core;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.reflections.Reflections;

import org.petrify.core.transition.TransitionFactory;
import org.petrify.core.transition.TransitionRunner;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toUnmodifiableList;

public final class Net {

	private static final Logger log = Logger.getLogger(Net.class.getSimpleName()); 

	private final TransitionFactory transitionFactory = TransitionFactory.get();

	private Map<String, Place> places = new TreeMap<>();

	private Map<String, List<Transition>> transitions = new LinkedHashMap<>();

	private Map<Transition, TransitionRunner> runners = new LinkedHashMap<>();

	private Net() {
	}
	
	public Net(Collection<Class<?>> actionClasses) {
		addTransitions(actionClasses);
	}

	public Net(String packageName) {
		this(new Reflections(packageName).getTypesAnnotatedWith(org.petrify.Transition.class));
	}

	private void addPlace(Place place) {
		places.put(place.getName(), place);
	}

	private void addTransitions(Collection<Class<?>> actionClasses) {
		for (Class<?> actionClass : actionClasses) {
			addTransition(actionClass);
		}
	}

	private void addTransition(Object instance) {
		Transition transition = transitionFactory.createTransition(instance, places);
		addTransition(transition);
	}

	private void addTransition(Transition transition) {
		this.transitions
				.computeIfAbsent(transition.getName(), (name) -> new LinkedList<>())
				.add(transition);
	}

	private void addTransition(Class<?> transitionClass) {
		try {
			Transition transition = transitionFactory.createTransition(transitionClass, places);
			addTransition(transition);
		} catch (Exception e) {
			throw new RuntimeException("unable to create transition", e);
		}
	}
	
	public Collection<Place> getPlaces() {
		return Collections.unmodifiableCollection(places.values());
	}
	
	public Place getPlace(Object key) {
		return places.get(key.toString());
	}
	
	public Collection<Transition> getTransitions() {
		return transitions.values().stream().flatMap(List::stream).collect(toUnmodifiableList());
	}

	public Collection<Transition> getTransitionsByName(String name) {
		return Collections.unmodifiableCollection(transitions.get(name));
	}

	public Net start() {
		transitions.values().stream()
			.flatMap(List::stream)
			.map(transition -> runners.compute(transition, (k, v) -> new TransitionRunner(k)))
			.forEach(Thread::start);
		return this;
	}
	
	public void stop() {
		runners.values().stream().peek(Thread::interrupt).forEach(this::join);
	}

	private void join(TransitionRunner transition) {
		try {
			transition.join();
		} catch (Exception e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	public Object get(Object key) throws InterruptedException {
		Place place = places.get(key.toString());
		return place.acquireAndConsume().getValue();
	}
	
	public Net put(Object key, Object value) throws InterruptedException {
		Place place = places.get(requireNonNull(key).toString());
		place.acquireAndProduce(new Token(value));
		return this;
	}
	
	public static class Builder {
		
		private List<Place> places = new ArrayList<>();
		
		private List<Object> actions = new ArrayList<>();
		
		private List<Class<?>> actionClasses = new ArrayList<>();
		
		public Builder place(Place place) {
			places.add(place);
			return this;
		}
		
		public Builder transition(Class<?> actionClass) {
			actionClasses.add(actionClass);
			return this;
		}
		
		public Builder transition(Object action) {
			actions.add(action);
			return this;
		}
		
		public Net build() {
			Net net = new Net();
			places.forEach(net::addPlace);
			actions.forEach(net::addTransition);
			actionClasses.forEach(net::addTransition);
			return net;
		}
	}
	
}
