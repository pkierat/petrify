/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.core;

import org.petrify.meta.MemberInfo;

import java.lang.annotation.Annotation;
import java.lang.invoke.LambdaMetafactory;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ReflectionUtils {

    private static final MethodHandles.Lookup lookup = MethodHandles.lookup();

	private ReflectionUtils() {
		super();
	}

	public static <T> T newInstance(Class<T> cls) {
		try {
			return cls.getConstructor().newInstance();
		} catch (InstantiationException
				| IllegalAccessException
				| NoSuchMethodException
				| InvocationTargetException e) {
			throw new RuntimeException(
					String.format("unable to create instance of class: %s", cls.getName()), e);
		}
	}
	

	private static <T> T newInstance(Class<T> cls, Class<?>[] argTypes, Object[] args) {
		try {
			return cls.getConstructor(argTypes).newInstance(args);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static <A extends Annotation> Optional<Method> getFirstAnnotatedMethod(Class<?> cls, Class<A> annotationType) {
		if (cls == null || cls == Object.class) { return Optional.empty(); }
		return getFirstAnnotatedMethod(cls.getDeclaredMethods(), annotationType)
			.or(() -> getFirstAnnotatedMethod(cls.getInterfaces(), annotationType))
			.or(() -> getFirstAnnotatedMethod(cls.getSuperclass(), annotationType));
	}

	private static <A extends Annotation> Optional<Method> getFirstAnnotatedMethod(Method[] methods, Class<A> annotationType) {
		return Arrays.stream(methods)
			.filter(m -> isAnnotatedWith(m, annotationType))
			.findFirst();
	}

	private static <A extends Annotation> Optional<Method> getFirstAnnotatedMethod(Class<?>[] classes, Class<A> annotationType) {
		return Arrays.stream(classes)
			.map(iface -> getFirstAnnotatedMethod(iface, annotationType))
			.findFirst().flatMap(Function.identity());
	}

	public static List<Method> getAllAnnotatedMethods(Class<?> cls, Class<? extends Annotation> annotationType) {
		return Arrays.stream(cls.getMethods())
				.filter(method -> isAnnotatedWith(method, annotationType))
				.collect(Collectors.toList());
	}

	private static boolean isAnnotatedWith(AnnotatedElement element, Class<? extends Annotation> annotationClass) {
		boolean present = element.isAnnotationPresent(annotationClass);
		if (!present) {
			present = Arrays.stream(element.getAnnotations())
				.anyMatch(anno -> anno.annotationType().isAnnotationPresent(annotationClass));
		}
		return present;
	}

	public static Object getAnnotationValue(Annotation anno, String attributeName) {
		try {
			return anno.annotationType().getMethod(attributeName).invoke(anno);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}	

	private static <T> T toLambda(Object transition, Method method, Class<T> funcClass, String funcName) {
		try {
			MethodHandle mh = lookup.unreflect(method);
			return toLambda(transition, mh,
					method.getParameterTypes(),
					method.getReturnType(),
					funcClass, funcName);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
	
	private static <T> T toLambda(Object transition, MethodHandle method,
								  Class<?>[] parameterTypes, Class<?> returnType,
								  Class<T> funcClass, String funcName) {
		try {
		    MethodType expectedType = MethodType.methodType(funcClass, transition.getClass());
		    Class<?>[] samArgs = Arrays.stream(parameterTypes).map(c -> Object.class).toArray(Class<?>[]::new);
		    MethodType samType = MethodType.methodType(void.class == returnType ? void.class : Object.class, samArgs);
		    MethodType instType = MethodType.methodType(returnType, parameterTypes);
		    Object func = LambdaMetafactory
		    		.metafactory(lookup, funcName, expectedType, samType, method, instType)
		    		.getTarget().invoke(transition);
			return funcClass.cast(func);
		} catch (Throwable e) {
			throw new RuntimeException(e);
		}		
	}
	
	@SuppressWarnings("unchecked")
	public static Consumer<Object> getConsumer(Object transition, Method method) {
		return toLambda(transition, method, Consumer.class, "accept");
	}
	
	@SuppressWarnings("unchecked")
	public static Supplier<Object> getSupplier(Object transition, Method method) {
		return toLambda(transition, method, Supplier.class, "get");
	}
	
	public static Runnable getAction(Object transition, Method method) {
		return toLambda(transition, method, Runnable.class, "run");
	}

	public static Function<Object[], Object> getFunction(Object transition, Method method) {
		try {
			final MethodHandle handle = lookup.unreflect(method)
					.bindTo(transition)
                    .asSpreader(Object[].class, method.getParameterCount());
			return (args) -> {
				try {
					return handle.invoke(args);
				} catch (Throwable e) {
					throw new RuntimeException(e);
				}
			};
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static <T, A extends Annotation> T decorate(Class<T> targetClass, T target,
			Class<A> decoratorAnnoClass, Annotation[] annotations, MemberInfo memberInfo) {
		T decorated = target;
		for (Annotation annotation : annotations) {
			Class<?> annotationType = annotation.annotationType();
			A tdAnno = annotationType.getAnnotation(decoratorAnnoClass);
			if (tdAnno != null) {
				Class<?> tdClass = (Class<?>) getAnnotationValue(tdAnno, "value");
				decorated = targetClass.cast(newInstance(tdClass,
						new Class<?>[] { targetClass, annotationType, MemberInfo.class },
						new Object[] { decorated, annotation, memberInfo }));
			}
		}
		return decorated;
	}

}
