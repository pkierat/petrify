/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.sample.settlers;

import org.petrify.Action;
import org.petrify.Transition;

@Transition
public class Bakery {

	private Water water;
	
	private Flour flour;
	
	private Bread bread;
	
	@Requires(ResourceType.FLOUR)
	public void setFlour(Flour flour) {
		this.flour = flour;
	}
	
	@Requires(ResourceType.WATER)
	public void setWater(Water water) {
		this.water = water;
	}
	
	@Provides(ResourceType.BREAD)
	public Bread getBread() {
		return bread;
	}
	
	@Action
	public void growAndHarvest() {
		bread = new Bread(flour, water);
	}
	
}
