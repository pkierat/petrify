/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.sample.settlers;

import javax.validation.constraints.NotNull;

import org.petrify.Action;
import org.petrify.Transition;
import org.petrify.extensions.Guarded;

@Transition
public class Mill {

	private Grain grain;
	
	private Flour flour;

	@Guarded
	@Requires(ResourceType.GRAIN)
	public void setGrain(@NotNull Grain grain) {
		this.grain = grain;
	}
	
	@Provides(ResourceType.FLOUR)
	public Flour getFlour() {
		return flour;
	}
	
	@Action
	public void grind() {
		flour = new Flour(grain);
	}
	
}
