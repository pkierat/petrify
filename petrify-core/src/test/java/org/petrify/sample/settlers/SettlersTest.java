/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.sample.settlers;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import org.petrify.core.Net;
import org.petrify.core.NetTest;
import org.petrify.core.Transition;

public class SettlersTest {

	private static final Logger log = Logger.getLogger(NetTest.class.getSimpleName());


	@BeforeClass
	public static void setUpClass() {
		System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");
	}

	@Test
//	@Ignore
	public void testSettlers() {
		final AtomicInteger counter = new AtomicInteger();
		final AtomicReference<Object> hole = new AtomicReference<>();
		new Thread(() -> {
			try {
				while (!Thread.currentThread().isInterrupted()) {
					TimeUnit.SECONDS.sleep(1);
					int value = counter.getAndSet(0);
					log.info("Produced: " + value);
				}
			} catch (InterruptedException e) {}
		}).start();
		Net net = new Net("org.petrify.sample.settlers").start();
		try {
			while (!Thread.interrupted()) {
				hole.set(net.get(ResourceType.IRON));
				counter.incrementAndGet();
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		}
	}

	@Test
	public void testSettlersInSequence() {
		Net net = new Net("org.petrify.sample.settlers");
		
		Transition farm = findTransition(net, "Farm");
		Transition well = findTransition(net, "Well");
		Transition mill = findTransition(net, "Mill");
		Transition bakery = findTransition(net, "Bakery");
		Transition brewery = findTransition(net, "Brewery");
		Transition ironMine = findTransition(net, "IronMine");
		
		try {
			testFarm(net, farm);
			testMill(net, mill);
			testWell(net, well);
			testBakery(net, bakery);
			testFarm(net, farm);
			testWell(net, well);
			testBrewery(net, brewery);
			testIronMine(net, ironMine);
		} catch (Exception e) {
			log.log(Level.SEVERE, e.getMessage(), e);
		} finally {
			net.stop();
			log.info("Net has stopped.");
		}
	}

	private void testIronMine(Net net, Transition ironMine) throws InterruptedException {
		assertTrue(ironMine.isEnabled());
		ironMine.fire();
		assertTrue(net.getPlace(ResourceType.BREAD).isEmpty());
		assertTrue(net.getPlace(ResourceType.BEER).isEmpty());
		assertFalse(net.getPlace(ResourceType.IRON).isEmpty());
	}

	private void testBrewery(Net net, Transition brewery) throws InterruptedException {
		assertTrue(brewery.isEnabled());
		brewery.fire();
		assertTrue(net.getPlace(ResourceType.GRAIN).isEmpty());
		assertTrue(net.getPlace(ResourceType.WATER).isEmpty());
		assertFalse(net.getPlace(ResourceType.BEER).isEmpty());
	}

	private void testBakery(Net net, Transition bakery) throws InterruptedException {
		assertTrue(bakery.isEnabled());
		bakery.fire();
		assertTrue(net.getPlace(ResourceType.FLOUR).isEmpty());
		assertTrue(net.getPlace(ResourceType.WATER).isEmpty());
		assertFalse(net.getPlace(ResourceType.BREAD).isEmpty());
	}

	private void testWell(Net net, Transition well) throws InterruptedException {
		assertTrue(well.isEnabled());
		well.fire();
		assertFalse(net.getPlace(ResourceType.WATER).isEmpty());
	}

	private void testMill(Net net, Transition mill) throws InterruptedException {
		assertTrue(mill.isEnabled());
		mill.fire();
		assertTrue(net.getPlace(ResourceType.GRAIN).isEmpty());
		assertFalse(net.getPlace(ResourceType.FLOUR).isEmpty());
	}

	private void testFarm(Net net, Transition farm) throws InterruptedException {
		assertTrue(farm.isEnabled());
		farm.fire();
		assertFalse(net.getPlace(ResourceType.GRAIN).isEmpty());
	}

	private Transition findTransition(Net net, String name) {
		return net.getTransitionsByName(name).iterator().next();
	}

}
