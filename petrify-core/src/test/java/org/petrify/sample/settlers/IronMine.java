/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.sample.settlers;

import org.petrify.Action;
import org.petrify.Transition;

import java.util.logging.Logger;

@Transition
public class IronMine {

	private static final Logger log = Logger.getLogger(IronMine.class.getName());

	private Bread bread;
	
	private Beer beer;
	
	private Iron iron;
	
	@Requires(ResourceType.BREAD)
	public void setBread(Bread bread) {
		this.bread = bread;
	}
	
	@Requires(ResourceType.BEER)
	public void setBeer(Beer beer) {
		this.beer = beer;
	}
	
	@Provides(ResourceType.IRON)
	public Iron getIron() {
		return iron;
	}
	
	@Action
	public void mine() {
		iron = new Iron(bread, beer);
	}
	
}
