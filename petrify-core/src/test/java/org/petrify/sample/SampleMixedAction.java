/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.sample;

import org.petrify.Action;
import org.petrify.Input;
import org.petrify.Output;
import org.petrify.Transition;

/**
 * Example of mapping of a POJO class to a transition with
 * additional places mapped to parameters and result
 * of the action method.
 * 
 * @author pawel_kierat
 */
@Transition
public class SampleMixedAction {

	private static final String INPUT1 = "mixed_input1";
	private static final String INPUT2 = "mixed_input2";
	private static final String OUTPUT1 = "mixed_output1";
	private static final String OUTPUT2 = "mixed_output2";

	private String inputString;

	private String outputString;
	
	@Input(INPUT1)
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	
	@Output(OUTPUT1)
	public String getOutputString() {
		return outputString;
	}

	@Action
	@Output(OUTPUT2)
	public Integer execute(@Input(INPUT2) Integer number) {
		outputString = inputString + "2";
		return number + 1;
	}
	
}
