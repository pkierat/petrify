/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.sample;

import org.petrify.Action;
import org.petrify.Input;
import org.petrify.Output;
import org.petrify.Transition;

/**
 * Example of mapping of a POJO class to a transition. 
 * 
 * @author pawel_kierat
 */
@Transition
public class SamplePojoAction {

	private static final String INPUT = "pojo_input";
	private static final String OUTPUT = "pojo_output";

	private String inputString;

	private String outputString;
	
	@Input(INPUT)
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	
	@Output(OUTPUT)
	public String getOutputString() {
		return outputString;
	}

	@Action
	public void execute() throws InterruptedException {
		outputString = inputString + "2";
	}
	
}
