/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.sample;

import javax.validation.constraints.Min;

import org.petrify.Action;
import org.petrify.Input;
import org.petrify.Output;
import org.petrify.Transition;

/**
 * Example of transition with a loop.
 * Transition has two output places one of which is also an input place.
 * Execution loop stops when the counter is less or equal to 0.<br>
 * 
 * TODO: Guards (like @Min) are not implemented yet so this transition
 * falls into infinite loop after reaching counter == 0.
 * 
 * @author pawel_kierat
 *
 */
@Transition
public class CyclicAction {
	
	private static final String COUNTER = "counter";
	private static final String MESSAGE = "message";

	private Integer counter;
	
	private String message;
	
	@Input(value = COUNTER)
	public void setCounter(@Min(1) Integer counter) {
		this.counter = counter;
	}
	
	@Output(COUNTER)
	public Integer getCounter() {
		return counter;
	}

	@Output(MESSAGE)
	public String getMessage() {
		return message;
	}
	
	@Action
	public void fire() {
		if (counter > 0) {
			counter -= 1;
			message = "Waiting...";
		} else {
			message = "Done!";
		}
	}
	
}
