/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.sample;

import org.petrify.Action;
import org.petrify.Input;
import org.petrify.Output;
import org.petrify.Transition;

/**
 * Sample mapping of a single method to a transition. Method parameters act
 * as input places and the result is mapped to an output place.
 * 
 * @author pawel_kierat
 */
@Transition
public class SampleFunctionalAction {

	private static final String INPUT = "func_input";
	private static final String OUTPUT = "func_output";

	@Action
	@Output(OUTPUT)
	public String execute(@Input(INPUT) String theString) {
		return theString + "1";
	}
	
}
