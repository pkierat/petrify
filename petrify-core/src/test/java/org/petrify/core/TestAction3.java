/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.core;

import org.petrify.Action;
import org.petrify.Input;
import org.petrify.Output;
import org.petrify.Transition;

@Transition(TestAction3.NAME)
public class TestAction3 {

	static final String NAME = "TestAction3";

	private Integer input1;

	private Integer input2;

	private Integer output;
	
	@Input(TestPlaces.INCREMENTED)
	public void setInput1(Integer input1) {
		this.input1 = input1;
	}

	@Input(TestPlaces.DECREMENTED)
	public void setInput2(Integer input2) {
		this.input2 = input2;
	}

	@Output(TestPlaces.SUM)
	public Integer getOutput() {
		return output;
	}
	
	@Action
	public void add() {
		output = input1 + input2;
	}
	
}
