/*
 * Copyright 2018 Pawel Kierat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.core;

import org.petrify.Action;
import org.petrify.Input;
import org.petrify.Output;
import org.petrify.Transition;

@Transition(TestAction2.NAME)
public class TestAction2 {

	static final String NAME = "TestAction2";

	private Integer input;

	private Integer output;
	
	private int count = 0;
	
	@Input(TestPlaces.INPUT2)
	public void setInput(Integer number) {
		this.input = number;
	}

	@Output(TestPlaces.DECREMENTED)
	public Integer getOutput() {
		return output;
	}

	public int getCount() {
		return count;
	}
	
	@Action
	public void decrement() {
		output = input - 1;
		count++;
	}
	
}
