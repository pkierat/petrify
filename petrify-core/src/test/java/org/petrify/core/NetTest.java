/*
 * Copyright 2018 Pawel Kierat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.petrify.core;

import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;

import org.petrify.core.place.PlaceFactory;

import static org.assertj.core.api.Assertions.assertThat;

public class NetTest {

    private static final Logger log = Logger.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    @BeforeClass
    public static void setUpClass() {
        System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");
    }

    @Test
    public void testNetBuildingFromClasses() {
        Net net = new Net(Arrays.asList(TestAction1.class, TestAction2.class, TestAction3.class));
        assertProperNetStructure(net);
    }

    @Test
    public void testNetBuildingFromPackage() {
        Net net = new Net("org.petrify.core");
        assertProperNetStructure(net);
    }

    @Test
    public void testNetBuildingUsingApi() {
        Net net = new Net.Builder()
                .place(PlaceFactory.createPlace(TestPlaces.INPUT1))
                .place(PlaceFactory.createPlace(TestPlaces.INPUT2))
                .transition(new TestAction1())
                .transition(new TestAction2())
                .transition(TestAction3.class)
                .build();
        assertProperNetStructure(net);
    }


    @Test(timeout = 1000)
    public void testRunNetAutomatically() {
        Net net = new Net(Arrays.asList(TestAction1.class, TestAction2.class, TestAction3.class));
        net.start();
        try {
            net.put(TestPlaces.INPUT1, 5).put(TestPlaces.INPUT2, 5);

            Integer output = (Integer) net.get(TestPlaces.SUM);

            assertThat(output).isEqualTo(10);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        } finally {
            net.stop();
            log.info("Net stopped.");
        }
    }

    @Test
    public void testRunNetManually() throws InterruptedException {
        Net net = new Net(Arrays.asList(TestAction1.class, TestAction2.class, TestAction3.class));

        Transition action1 = findTransition(net, TestAction1.NAME);
        Transition action2 = findTransition(net, TestAction2.NAME);
        Transition action3 = findTransition(net, TestAction3.NAME);

        net.put(TestPlaces.INPUT1, 5);
        net.put(TestPlaces.INPUT2, 5);

        action1.fire();
        action2.fire();
        action3.fire();

        Integer output = (Integer) net.get(TestPlaces.SUM);
        assertThat(output).isEqualTo(10);
    }

    private void assertProperNetStructure(Net net) {
        assertThat(net.getPlaces())
                .hasSize(5)
                .extracting(Place::getName)
                .containsOnly(
                        TestPlaces.INPUT1,
                        TestPlaces.INPUT2,
                        TestPlaces.INCREMENTED,
                        TestPlaces.DECREMENTED,
                        TestPlaces.SUM);

        assertThat(net.getTransitions())
                .hasSize(3)
                .extracting(Transition::getName)
                .containsOnly(
                        TestAction1.NAME,
                        TestAction2.NAME,
                        TestAction3.NAME);

        Transition action1 = findTransition(net, TestAction1.NAME);
        assertThat(action1).isNotNull();
        assertValidArcs(action1.getInputArcs(), 1, TestPlaces.INPUT1);
        assertValidArcs(action1.getOutputArcs(), 1, TestPlaces.INCREMENTED);

        Transition action2 = findTransition(net, TestAction2.NAME);
        assertThat(action2).isNotNull();
        assertValidArcs(action2.getInputArcs(), 1, TestPlaces.INPUT2);
        assertValidArcs(action2.getOutputArcs(), 1, TestPlaces.DECREMENTED);

        Transition action3 = findTransition(net, TestAction3.NAME);
        assertThat(action3).isNotNull();
        assertValidArcs(action3.getInputArcs(), 2, TestPlaces.INCREMENTED, TestPlaces.DECREMENTED);
        assertValidArcs(action3.getOutputArcs(), 1, TestPlaces.SUM);
    }

    private void assertValidArcs(List<? extends Arc> arcs, int expectedSize, String... placeNames) {
        assertThat(arcs)
                .hasSize(expectedSize)
                .extracting(Arc::getPlace)
                .extracting(Place::getName)
                .containsOnly(placeNames);
    }

    private Transition findTransition(Net net, String name) {
        return net.getTransitionsByName(name).iterator().next();
    }

}
