Summary:
--------
This is a proof-of-concept implementation of a framework for declarative
(annotation-based) mapping of simple Java classes to Petri Nets and running
them in concurrent environment without the need of manual synchronization.

The basic idea is to:
* map methods to transitions
* map setters and method parameters to input arcs
* map getters and method results to output arcs
* generate place for each arc
* run the code in concurrent environment by running each transition in
  separate thread

Currently the implementation is compatible with the semantics of
Generalized Petri Nets, though some extensions can be easily provided
by the means of pluggable place, arc and transition decorators (see:
com.epam.petri.extensions.Timed annotation).

CAUTION:
--------
This project is in early-pre-alpha/proof-of-concept stage. It may:
* not work correctly at all,
* crash in random situations,
* perform very poorly,
* eat your dog and burn your house.

__DO NOT__ use it in production environment.